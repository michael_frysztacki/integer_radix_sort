#include <iostream>
#include <bitset>
#include <ctime>
#include <limits>
#include <cstdlib>
#include <ctime>

using namespace std;

/*
 * helper functions
 * ================
 */
template<typename T>
void printTab(T tab[], int size) {

    for (int i = 0; i < size; i++) {
       cout << tab[i] << ", "; 
    }
    cout << "\n";
}
void printCountTab(int count[], int size) {

    for (int i = 0; i < size; i++) {
        if(count[i] != 0) {
            cout << "idx: " << i << "value: " << count[i] << endl;
        }
    }
    cout << "\n";
}
void printTabInBinary(int tab[], int size) {
    for (int i = 0; i < size; i++) {
        bitset<32> x(tab[i]);
        cout << x << endl;
    }
}
template<int N>
struct PowerOfTwo {
    enum {
        value = 2 * PowerOfTwo<N-1>::value
    };
};
template<>
struct PowerOfTwo<1> {
    enum {
        value = 2
    };
};
/* 
 * end of helper functions
 * =======================
 */
typedef int (*MapFunc)(int);
void sortIntegersByPart(int tab[], int size, MapFunc fun);

int higherPartMap(int val) {
    int ret = val >> 16; 
    if (val < 0) {
       ret = ret | 0xffff0000;
       return ret;
    } else {
        ret = ret & 0x0000ffff;
        return ret;
    }
    // int ret = val / PowerOfTwo<16>::value;
    // return ret;
};

int lowerPartMap(int val) {
    if (val < 0) {
       int ret = 0xffff0000 | val; 
       return ret;
    } else {
        int ret = 0x0000ffff & val; 
        return ret;
    }
    // return (val < 0 ? 0xffff0000 | val : 0x0000ffff & val);
};
/*
 * main radix sorting function 
 */
void lsd_sort(int tab[], const int size) {
    //sort integers by lower 16 bits
    sortIntegersByPart(tab, size, lowerPartMap);
    //sort integers by higher 16 bits
    sortIntegersByPart(tab, size, higherPartMap);
}
/*
 * function which sorts integers by part specified in 'fun'
 */
void sortIntegersByPart(int tab[], int size, MapFunc fun) {

    const int R = PowerOfTwo<17>::value;
    int count[R+1] = {0};
    for (int i = 0; i < size; i++) {
        count[fun(tab[i]) + PowerOfTwo<16>::value+1]++;
    }
    for (int i = 0; i < R; i++) {
        count[i+1] += count[i]; 
    }
    int* aux = new int[size];
    for (int i = 0; i < size; i++) {
        aux[count[fun(tab[i]) + PowerOfTwo<16>::value]++] = tab[i];
    }
    for (int i = 0; i < size; i++) {
        tab[i] = aux[i];
    }
    delete aux;
}


/*
 * function to test array after sorting
 */
bool isSorted(int tab[], int size) {

    for (int i = 0; i < size-1; i++) {
       if(tab[i] > tab[i+1]) {
           cout << "idx i:   " << i << "value: " << tab[i] << endl;
           cout << "idx i+1: " << i+1 << "value: " << tab[i+1] << endl;
           return false;
       } 
    }
    return true;
}
int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

/*
 * helper function to generate an array of integers for experiment
 */
int* generateTab(int size) {

    srand(time(0));
    int* testTab = new int[size];
    for (int i = 0; i < size; i++) {
        int rand_int = rand();
        rand_int = rand_int * (rand() % 2 == 0 ? 1 : -1);
        testTab[i] = rand_int;
    }
    return testTab;
}

/*
 * run radix sort experiment on specified 'size' of integers.
 */
float lsd_radix_experiment(int size) {

    srand(time(0));
    int* testTab = generateTab(size);
    const clock_t begin_time = clock();
    lsd_sort(testTab, size);
    cout << "is sorted: " << isSorted(testTab, size) << endl;
    const clock_t stop_time = clock();
    return float( stop_time - begin_time ) /  CLOCKS_PER_SEC;
}

/*
 * run quicksort experiment on 'size' integers to compare time with radix experiment
 */
float quicksort_experiment(int size) {

    std::srand(std::time(0));
    int* testTab = generateTab(size);
    const clock_t begin_time = clock();
    qsort(testTab, size, sizeof(int), compare);
    const clock_t stop_time = clock();
    return float( stop_time - begin_time ) /  CLOCKS_PER_SEC;
}

/*
 * it seems that radix sort is faster/same than quicksort when sorting 10000 integers !
 */
int main()
{
    const int tenThousand = 100000;
    const float quick_result = quicksort_experiment(tenThousand);
    const float lsd_result = lsd_radix_experiment(tenThousand);
    cout << "lsd radix sort time : " << lsd_result << endl;
    cout << "quicksort time: " << quick_result << endl;
    return 0;
}
